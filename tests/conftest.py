import numpy as np
import numpy.typing as npt
import pytest
from matchms import Spectrum

ARRAY_TYPE = npt.NDArray[np.float64]


@pytest.fixture
def mz() -> ARRAY_TYPE:
    return np.array([10, 20, 30, 40], dtype="float")


@pytest.fixture
def intensities() -> ARRAY_TYPE:
    return np.array([0, 1, 10, 100], dtype="float")


@pytest.fixture
def spectrum(mz: ARRAY_TYPE, intensities: ARRAY_TYPE) -> Spectrum:
    return Spectrum(mz=mz, intensities=intensities)


@pytest.fixture
def spectrum_empty() -> Spectrum:
    mz = np.array([], dtype="float")
    intensities = np.array([], dtype="float")
    return Spectrum(mz=mz, intensities=intensities)
