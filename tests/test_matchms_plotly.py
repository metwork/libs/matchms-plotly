from pathlib import Path
from matchms import Spectrum
from matchms_plotly import __version__, plot_spectrum, plot_spectra
from plotly.graph_objects import Figure


def test_version() -> None:
    toml_path = Path(__file__).parents[1] / "pyproject.toml"
    version_line = toml_path.read_text().split("\n")[2]
    version = version_line.split(" = ")[1]
    assert version == f'"{__version__}"'


def test_plot_spectrum(spectrum: Spectrum) -> None:
    figure = plot_spectrum(spectrum)
    assert isinstance(figure, Figure)


def test_plot_spectrum_empty(spectrum_empty: Spectrum) -> None:
    figure = plot_spectrum(spectrum_empty)
    assert isinstance(figure, Figure)


def test_plot_spectra(spectrum: Spectrum) -> None:
    figure = plot_spectra(spectrum, spectrum)
    assert isinstance(figure, Figure)
